package org.fcc.guangzhou.cache;

import org.fcc.guangzhou.cache.impl.UserGoodsCacheMemory;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 */
public class UserGoodsCacheTest {

    @Test
    public void testUserGoodsCacheMemory() {
        UserGoodsCache cache = new UserGoodsCacheMemory();
        cache.buy("test1");
        cache.buy("test2");

        assertEquals(Integer.valueOf(1), cache.getBoughtNum("test1"));
        assertEquals(Integer.valueOf(1), cache.getBoughtNum("test2"));
        assertNull(cache.getBoughtNum("test3"));

        assertFalse(cache.hasRemainGoods(2));
        assertTrue(cache.hasRemainGoods(3));
    }
}
