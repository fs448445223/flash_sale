package org.fcc.guangzhou.dao;

import java.io.File;
import java.util.Map;

import org.fcc.guangzhou.dao.impl.UserGoodsDAOFile;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Wu Yuping
 */
public class UserGoodsDAOTest {

    @Test
    public void testUserGoodsDAOFile() {
        UserGoodsDAO userGoodsDAO = new UserGoodsDAOFile();
        userGoodsDAO.read();

        userGoodsDAO.buy("test1", 1);
        userGoodsDAO.buy("test2", 1);

        Map<String, Integer> result = userGoodsDAO.read();
        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals(Integer.valueOf(1), result.get("test1"));
        assertEquals(Integer.valueOf(1), result.get("test2"));
    }

    @After
    public void cleanUp() {
        File file = new File("store.txt");
        file.delete();
    }
}
