package org.fcc.guangzhou.service.impl;

import java.util.Map;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.fcc.guangzhou.cache.UserGoodsCache;
import org.fcc.guangzhou.dao.UserGoodsDAO;
import org.fcc.guangzhou.service.BuyService;
import org.springframework.stereotype.Service;

/**
 * @author Wu Yuping
 */
@Service
public class BuyServiceImpl implements BuyService {

    @Override
    public synchronized boolean buy(String userId) {
        Integer boughtNum = this.userGoodsCache.getBoughtNum(userId);
        if (null != boughtNum && boughtNum >= this.maxNumPerUser) {
            return true;
        }

        if (this.userGoodsCache.hasRemainGoods(this.goodsNum)) {
            this.userGoodsDAO.buy(userId, null == boughtNum ? 1 : boughtNum + 1);
            this.userGoodsCache.buy(userId);
            return true;
        }

        return false;
    }

    @PostConstruct
    public void initialize() {
        Map<String, Integer> map = this.userGoodsDAO.read();
        map.forEach((key, value) -> {
            for (int i = 0; i < value; i += 1) {
                userGoodsCache.buy(key);
            }
        });
    }

    private int maxNumPerUser = 1;
    private int goodsNum = 10;

    public void setMaxNumPerUser(int maxNumPerUser) {
        this.maxNumPerUser = maxNumPerUser;
    }

    public void setGoodsNum(int goodsNum) {
        this.goodsNum = goodsNum;
    }

    private UserGoodsCache userGoodsCache;
    private UserGoodsDAO userGoodsDAO;

    @Inject
    public void setUserGoodsCache(UserGoodsCache userGoodsCache) {
        this.userGoodsCache = userGoodsCache;
    }

    @Inject
    public void setUserGoodsDAO(UserGoodsDAO userGoodsDAO) {
        this.userGoodsDAO = userGoodsDAO;
    }
}
