package org.fcc.guangzhou.service;

/**
 * @author Wu Yuping
 */
public interface BuyService {

    boolean buy(String userId);
}
