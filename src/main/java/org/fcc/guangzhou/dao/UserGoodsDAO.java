package org.fcc.guangzhou.dao;

import java.util.Map;

/**
 */
public interface UserGoodsDAO {

    Map<String, Integer> read();

    void buy(String userId, int num);
}
