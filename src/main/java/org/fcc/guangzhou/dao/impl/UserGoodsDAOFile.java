package org.fcc.guangzhou.dao.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.fcc.guangzhou.dao.UserGoodsDAO;
import org.springframework.stereotype.Repository;

/**
 */
@Repository
public class UserGoodsDAOFile implements UserGoodsDAO {

    @Override
    public Map<String, Integer> read() {
        try {
            Map<String, Integer> result = new HashMap<>();
            if ( !(new File(fileName).exists()) ) {
                return result;
            }

            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
                    new FileInputStream(fileName), StandardCharsets.UTF_8))) {
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    int seperatorIndex = line.indexOf(':');
                    if (seperatorIndex <= 0) {
                        continue;
                    }

                    int num = Integer.valueOf(line.substring(0, seperatorIndex));
                    String userId = line.substring(seperatorIndex + 1);
                    result.put(userId, num);
                }
            }
            return result;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void buy(String userId, int num) {
        try {
            try (PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(
                    new FileOutputStream(fileName, true), StandardCharsets.UTF_8))) {
                printWriter.println(num + ":" + userId);
            }
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private static final String fileName = "store.txt";
}
