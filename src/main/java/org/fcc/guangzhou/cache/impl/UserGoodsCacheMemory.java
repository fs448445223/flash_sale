package org.fcc.guangzhou.cache.impl;

import java.util.HashMap;
import java.util.Map;

import org.fcc.guangzhou.cache.UserGoodsCache;
import org.springframework.stereotype.Repository;

/**
 */
@Repository
public class UserGoodsCacheMemory implements UserGoodsCache {

    @Override
    public Integer getBoughtNum(String userId) {
        return this.cache.get(userId);
    }

    @Override
    public boolean hasRemainGoods(int totalGoodsNum) {
        return this.cache.size() < totalGoodsNum;
    }

    @Override
    public void buy(String userId) {
        Integer num = this.cache.get(userId);
        if (null == num) {
            this.cache.put(userId, 1);
        } else {
            this.cache.put(userId, num + 1);
        }
    }

    private final Map<String, Integer> cache = new HashMap<>();
}
