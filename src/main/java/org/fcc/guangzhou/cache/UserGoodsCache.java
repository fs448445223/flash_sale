package org.fcc.guangzhou.cache;

/**
 * @author Wu Yuping
 */
public interface UserGoodsCache {

    Integer getBoughtNum(String userId);

    boolean hasRemainGoods(int totalGoodsNum);

    void buy(String userId);
}
