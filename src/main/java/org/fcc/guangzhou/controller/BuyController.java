package org.fcc.guangzhou.controller;

import javax.inject.Inject;

import org.fcc.guangzhou.service.BuyService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 */
@RestController
@RequestMapping("/buy")
public class BuyController {

    private BuyService buyService;
    @Inject
    public void setBuyService(BuyService buyService) {
        this.buyService = buyService;
    }

    @PostMapping
    public boolean buy(String userId) {
        return buyService.buy(userId);
    }
}
